---
layout: home
title: Hello
---

I'm a developer based in Tokyo, JAPAN, originally from [Torun, POLAND](http://www.torun.pl/en).

I wear many hats.

Sometimes I'm a matematician. I love the aspect of math where I get to solve a puzzle. The answer is out there, dictated by the laws of logic.

I can be creative. I enjoy making data easy to understand and pleasant for the eyes. I'm driven by making user-focused products. Nothing makes me more proud than the thought that something I created is useful to someone. 

I believe that technology, be it hardware, software or services should be designed in a way to improve, rather than replace, human beings. Enhance our senses, sharpen our memory and focus our thoughts. 

> “When we invented the personal computer,<br>
> we created a new kind of bicycle... <br>
> a new man-machine partnership... <br>
> a new generation of entrepreneurs.” <br>
> — Steve Jobs, c. 1980
 
